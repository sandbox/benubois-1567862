<?php
/**
* Implements hook_token_info().
*/
function entity_embed_token_info() {
  $types['embed'] = array(
    'name' => t("Entity Embed Tokens"),
    'description' => t("Tokens for embedding entities in fields."),
  );
  $embed['node'] = array(
    'name' => t("Node"),
    'description' => t("Embed a node like [embed:node:nid]"),
  );
  $embed['view'] = array(
    'name' => t("View"),
    'description' => t("Embed a view like [embed:view:view_name:args]"),
  );
  
  return array(
    'types' => $types,
    'tokens' => array(
      'embed' => $embed,
    ),
  );
}

/**
* Implements hook_tokens().
*/
function entity_embed_tokens($type, $tokens, $data = array(), $options = array()) {
  $replacements = array();
	
  if ('embed' === $type) {
    foreach ($tokens as $token_data => $original_token) {
      
      $replacements[$original_token] = '';
      $args = explode(':', $token_data);

      $entity_type = array_shift($args);
			
      if ('node' === $entity_type) {
        
        if (isset($args[2]))
        {
          $args[2] = ('true' === strtolower($args[2])) ? TRUE : FALSE;
        }
        
        $node = call_user_func_array('node_load', $args);
        if ($node) {
          $node = node_show($node);
          $replacements[$original_token] = drupal_render($node);
        }
      }
      elseif ('view' === $entity_type) {
        $replacements[$original_token] = call_user_func_array('views_embed_view', $args);
      }
      
    }
  }
	
  return $replacements;
}

/**
 * Implements hook_help().
 */
function entity_embed_help($path, $arg) {
  switch ($path) {
    case 'admin/help#entity_embed':
    $output = '';
    
    $output = '<h3>' . t('About') . '</h3>';
    $output .= '<p>' . t('Entity Embed is a module that makes views and nodes available as tokens.') . '</p>';
    $output .= '<p>' . t('These tokens can be used in any field that can use an output filter like Body or other textareas.') . '</p>';
    $output .= '<h3>' . t('Installation') . '</h3>';
    $output .= '<p>' . t('Entity embed requires <a href="http://drupal.org/project/token_filter">Token Filter</a>. Token Filter must be enabled on each text format you want to use the module with. For example to turn Token Filter on for Full HTML visit /admin/config/content/formats/full_html and check "Replace tokens."') . '</p>';
    $output .= '<h3>' . t('Usage') . '</h3>';
    $output .= '<p>' . t('For <strong>nodes</strong> the syntax is <code>[embed:node:<em>nid</em>]</code> where <code>nid</code> is the node id of the node you want to embed. The module calls <a href="http://api.drupal.org/api/drupal/modules%21node%21node.module/function/node_load/7">node_load</a>, so it accepts the same arguments that node_load accepts.') . '</p>';
    $output .= '<p>' . t('For <strong>views</strong> the syntax is <code>[embed:view:<em>view_name</em>:<em>display_id</em>:<em>args</em>]</code> where <code>view_name</code> is the machine name of the view you want to embed, <code>display_id</code> is the display id you want to embed i.e. block_1 and <code>args</code> are any additional arguments you want to pass to contextual filters. The <code>args</code> should be colon separated.') . '</p>';
    return $output;
    
  }
}
